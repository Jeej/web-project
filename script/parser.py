from tqdm import tqdm
import json
import extract_keywords_from_abstract
from find_rank import find_rank

# CONST
core_path = "data/CORE_SET/core.json"
Threshold_citation = 10

#FUNC
def simplify(keywords):
    output = []
    for elt in keywords:
        output.append(elt.lower())
    return list(dict.fromkeys(output))

def parse_file(input_path,output_path):
    with open(input_path, 'rb') as input, open(output_path, 'w') as output, open(core_path, 'rb') as core:
        core_journals = json.load(core)
        for line in tqdm(input.readlines()):
            metadata_dict = json.loads(line)
            nb_cit = len(metadata_dict["inbound_citations"])
            if (nb_cit < Threshold_citation):
                continue;
            authors = metadata_dict['authors']
            title = metadata_dict["title"]
            year = metadata_dict["year"]
            doi = metadata_dict["doi"]
            journal = metadata_dict["journal"]
            abstract = metadata_dict["abstract"]
            if abstract:
                keywords = extract_keywords_from_abstract.extract_keywords(abstract) + extract_keywords_from_abstract.extract_keywords(title)
            else:
                keywords = extract_keywords_from_abstract.extract_keywords(title)

            keywords = simplify(keywords)

            if journal:
                rank,core_journal_name = find_rank(journal, core_journals)
            else:
                rank = "none"
                core_journal_name = "none"

            article = {"title":title, "authors":authors, "year":year, "journal":journal, "nb_cit":nb_cit, "doi":doi,"keywords":keywords, "rank":rank,"core_journal_name":core_journal_name}
            output.write(json.dumps(article, separators=(',',':')) + "\n")

# CALLS
for i in range(1,100):
    input_path = "data/metadata/Computer Science/metadata_" + str(i) + ".jsonl"
    output_path = "data/metadata_parsed/metadata" + str(i) + ".json"
    parse_file(input_path,output_path)
