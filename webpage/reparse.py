from tqdm import tqdm
import json
import os

#FUNC

def parse_file(input_path,output_path):
    with open(input_path, 'r') as input, open(output_path, 'w') as output:
        output.write("articles=[");
        for line in tqdm(input.readlines()):

            output.write(line + ",")
            #output.write(article)
            #print(authors[0])
            #for author in authors:
                #find_author_in_db(author)
                #add_article(author,line)

        output.seek(output.tell() - 2, os.SEEK_SET);
        output.write(']');
    return

# CALLS

# for i in range(1,100):
#     input_path = "metadata/Computer Science/metadata_" + str(i) + ".jsonl"
#     output_path = "metadata_parsed/metadata" + str(i) + ".json"
#     parse_file(input_path,output_path)

input_path = "database.json"
output_path = "database2.json"

parse_file(input_path, output_path);
