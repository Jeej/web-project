from tqdm import tqdm
import json
from collections import Counter

core_path = "data/CORE_SET/core.json"

#FUNC
def simplify(keywords):
    output = []
    for elt in keywords:
        output.append(elt.lower())
    return list(dict.fromkeys(output))

def parse_file(input_paths, output_path):
    core_journals2 = {}
    with open(core_path, 'r') as core:
        core_journals = json.load(core)
        for journal in core_journals:
            core_journals2[journal["name"]] = {"rank":journal["rank"], "acronym":journal["acronym"], "keywords":[]}
    for input_path in input_paths:
        with open(input_path, 'rb') as input:
            for line in tqdm(input.readlines()):
                metadata_dict = json.loads(line)
                keywords = metadata_dict["keywords"]
                core_journal_name = metadata_dict["core_journal_name"]
                rank = metadata_dict["rank"]
                if core_journal_name != "none":
                    core_journals2[core_journal_name]["keywords"] += keywords
    with open(output_path, 'w') as output:
        for journal_name in core_journals2:
            journal = core_journals2[journal_name]
            if journal["keywords"]:
                keywords = Counter(journal["keywords"])
            else:
                keywords = []
            # for keyword in keywords:
            #     if keywords["keywords"] == 1:
            #         del keywords["keywords"]
            new_journal = {"name":journal_name, "rank":journal["rank"], "acronym":journal["acronym"], "keywords":keywords}
            output.write(json.dumps(new_journal, separators=(',',':')) + "\n")


# CALLS

input_paths = []
for i in range(0,100):
    input_path = "data/metadata_parsed/metadata" + str(i) + ".json"
    input_paths.append(input_path)

output_path = "data/conference_data.json"

parse_file(input_paths,output_path)
