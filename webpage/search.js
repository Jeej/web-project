const path_db  = "database2.json"

function inside(str,table) {
  for(var i=0; i < table.length; i++) {
    if (str==table[i]) {
      return true;
    }
  return false;
  }
}

function find_articles(keywords) {
  // if(!Array.isArray(articles)) {
  //   console.log("Error in database : expect array in of articles, got " + typeof(articles));
  // }

  //TODO : check keywords is of good type

  output = []

  for(var i=0; i < articles.length; i++) {
    if (inside("article",articles[i].keywords)) {
      output.push(articles[i]);
    }
  }

  pp(output) // todo : change to pp(output) when output is calculated

  return;
}

function pp_authors(authors) {
  text = "<i> "
  for (var i=0; i< authors.length; i++) {
    author = authors[i]
    author_text = author["first"] + " "
    if (author["middle"]) {
      author_text += author["middle"] + " "
    }
    author_text += author["last"]
    if (author["suffix"]) {
      author_text += " " + author["suffix"]
    }
    text += author_text + ", "
  }
  text += "</i>"
  return text
}

function pp_articles(articles) {
  var readable_text = "";
  for (var i = 0; i < Math.min(articles.length,10); i++) {
      article = articles[i]
      article_name = article.title;
      readable_text += '<div class="a' + i + '" id="i' + i+1 + '">' + i + '.<b>'  + article_name + '</b>, ' + pp_authors(article["authors"]) + article["year"] + ". " + article["nb_cit"] + " citations." + '</div>' + '\n';
    }
  return readable_text;
}

function pp(articles) {
  // where we pretty print
  var node = document.getElementById('articles_list');

  var readable_text = pp_articles(articles);
  // update html
  node.innerHTML = readable_text;

}
