from tqdm import tqdm
import json
import csv
import os

input_path1 = "CORE_conf.csv"
input_path2 = "CORE_journals.csv"
output_path = "core.json"

with open(input_path1, 'r') as input1, open(input_path2, 'r') as input2, open(output_path, 'w') as output:
    output.write("[")
    csv_reader1 = csv.reader(input1, delimiter=',')
    csv_reader2 = csv.reader(input2, delimiter=',')
    for row in csv_reader1:
        if len(row) >= 5 and (row[4] == "A*" or row[4] == "A"):
            name = row[1]
            acronym = row[2]
            rank = row[4]
            journal = {"name":name,"acronym":acronym,"rank":rank}
            output.write(json.dumps(journal, separators=(',',':')) + ",\n")
    for row in csv_reader2:
        if len(row) >= 4 and (row[3] == "A*" or row[3] == "A"):
            name = row[1]
            acronym = "None"
            rank = row[3]
            journal = {"name":name,"acronym":acronym,"rank":rank}
            output.write(json.dumps(journal, separators=(',',':')) + ",\n")
    output.seek(output.tell() - 2, os.SEEK_SET)
    output.write(']')
