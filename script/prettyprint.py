
def pp_authors(authors):
  text = "<i> "
  for author in authors:
    author_text = author["first"] + " "
    author_text += author["last"]
    text += author_text + ", "
  text += "</i>"
  return text


def pp_articles(articles):
  readable_text = "";
  for i in range(len(articles)):
      article = articles[i]
      article_name = article["title"];
      readable_text += '<div class="a' + str(i) + '" id="i' + str(i+1) + '">' + str(i+1) + '.<b>'  + article_name + '</b>, ' + pp_authors(article["authors"]) + str(article["year"]) + ". " + str(article["nb_cit"]) + " citations." + '</div>' + '\n';
  return readable_text;

def pp_confs(confs):
  readable_text = "";
  for i in range(len(confs)):
      conf = confs[i]
      conf_name = conf["name"];
      conf_acronym = conf["acronym"];
      readable_text += '<div class="a' + str(i) + '" id="i' + str(i+1) + '">' + str(i+1) + '.<b>'  + conf_name + " (" + conf_acronym + ")" + '</b>, ranked ' + conf["rank"] + ". Keyword appeared in " + str(conf["occurence"]) + " different articles." + '</div>' + '\n';
  return readable_text;


def pp(articles, path, keywords):
    text = pp_articles(articles)
    with open(path, 'w') as output:
        html_text = """<!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8" />
            <title>Search result</title>
        </head>
        <style>
        .content {
          max-width : 630px;
          margin:auto;
        }
        p {
          margin-left:20px;
        }
        </style>
        <body>
        <div class="content">
        <h1>Search result: """ + " ".join(keywords) + """</h2>
          <div class="articles_list" id="articles_list">""" + text + """</div>
        </div>
        </body>
        </html>"""
        output.write(html_text)

def pp_conf(confs, path, keyword):
    text = pp_confs(confs)
    with open(path, 'w') as output:
        html_text = """<!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8" />
            <title>Search result</title>
        </head>
        <style>
        .content {
          max-width : 630px;
          margin:auto;
        }
        p {
          margin-left:20px;
        }
        </style>
        <body>
        <div class="content">
        <h1>Search result: """ + keyword + """</h2>
          <div class="articles_list" id="articles_list">""" + text + """</div>
        </div>
        </body>
        </html>"""
        output.write(html_text)
