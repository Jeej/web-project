from tqdm import tqdm
import json
import prettyprint
import sys

input_paths = ["data/metadata_parsed/metadata" + str(i) + ".json" for i in range(0,100)]

def search_articles(searched_keyword_list, number_elt_max):
    articles_found = []
    for path in input_paths:
        with open(path, 'rb') as input:
            nb_articles = 0
            for line in tqdm(input.readlines()):
                metadata_dict = json.loads(line)
                keywords = metadata_dict["keywords"]
                if searched_keyword_list.issubset(keywords):
                    nb_articles+=1
                    articles_found.append(metadata_dict)

    def get_nb_citation(article):
        return article["nb_cit"];

    articles_found.sort(key=get_nb_citation, reverse=True)
    limit = min(number_elt_max, len(articles_found)-1)
    articles_found = articles_found[:limit]

    return articles_found

if (len(sys.argv) >= 1):
    max = min(int(sys.argv[1]), 100)
    articles_list = search_articles(set(sys.argv[2:]), max)
    prettyprint.pp(articles_list,"output/article_search_output.html", sys.argv[2:])
