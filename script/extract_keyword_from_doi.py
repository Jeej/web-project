from tqdm import tqdm
import json
import subprocess
import os



def extract(str):
    for i in range(len(str)):
        if str[i] == "{":
            break;
    for j in range(len(str)-1,0,-1):
        if str[j] == "}":
            break;
    return str[i:j+1];



def extract_keywords(doi):
    url = "doi.org/" + doi;
    html_path = "output/temp_html" + ".txt"

    # Getting the data
    cmd = ["wget", "-O", html_path, url]
    subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=False)
    cmd2 = ["grep", '"keywords"', html_path]
    grep_output = subprocess.run(cmd2, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=False)

    data_unparsed = str(grep_output.stdout);
    data = extract(data_unparsed);

    if (not data):
        print("An error occured when trying to extract the keywords : couldn't find them.")

    # Parsing the data
    metadata_dict = json.loads(data)
    keywords = metadata_dict['keywords']
    for i in keywords:
        print(i['kwd'])
    #print(keywords)

    # Removing useless file
    os.remove(html_path)


#extract_keywords("10.1109/TAC.2015.2495174")
