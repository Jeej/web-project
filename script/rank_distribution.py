from tqdm import tqdm
import json
import matplotlib.pyplot as plt
from statistics import mean



input_path = "data/metadata_parsed/metadata0.json"

data = [0,0,0]

nb_citation_a_star = []

with open(input_path, 'rb') as input:
    for line in tqdm(input.readlines()):
        metadata_dict = json.loads(line)
        rank = metadata_dict["rank"]
        if rank == "A*":
            data[0]+=1
            nb_citation_a_star.append(metadata_dict["nb_cit"])
        elif rank == "A":
            data[1]+=1
        elif rank == "none":
            data[2]+=1
        else:
            print(rank)
        #output.write(str(nb_cit) + "\n")
        #output.write(article)
        #print(authors[0])
        #for author in authors:
            #find_author_in_db(author)
            #add_article(author,line)

print(data)

filtred_data = [0 for i in range(101)]

for elt in nb_citation_a_star:
    if elt>100:
        filtred_data[100]+=1
    else:
        filtred_data[elt]+=1

print(filtred_data)



# plotting the data
#bins=[1,10,20,30,40,50,60,70,80,90,100]

plt.figure(1)
plt.plot(["A*", "A", "None"], data)
plt.xlabel('Number of articles')
plt.ylabel('Rank')
plt.title('Distribution of the rank of the articles')

plt.figure(2)
plt.plot(filtred_data)
plt.xlabel('Number of articles')
plt.ylabel('Number of citations')
plt.title('Distribution of the number of citations of A* articles(top right is 100+)')

plt.show()
