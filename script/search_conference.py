from tqdm import tqdm
import json
import prettyprint
import sys

input_path = "data/conference_data.json"

def search_conf(searched_keyword, number_elt_max):
    conf_found = []
    with open(input_path, 'rb') as input:
        for line in tqdm(input.readlines()):
            metadata_dict = json.loads(line)
            keywords = metadata_dict["keywords"]
            if searched_keyword in list(keywords):
                conf_found.append({"name":metadata_dict["name"], "rank":metadata_dict["rank"], "occurence":metadata_dict["keywords"][searched_keyword], "acronym":metadata_dict["acronym"]})

    def get_nb_keywords(conf):
        return conf["occurence"];

    conf_found.sort(key=get_nb_keywords, reverse=True)
    limit = min(number_elt_max, len(conf_found)-1)
    conf_found = conf_found[:limit]

    return conf_found

if (len(sys.argv) >= 1):
    max = min(int(sys.argv[1]), 100)
    conf_list = search_conf(sys.argv[2], max)
    prettyprint.pp_conf(conf_list, "output/conf_search_output.html", sys.argv[2])
