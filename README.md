# About

This project use the [s2orc dataset](https://github.com/allenai/s2orc/) and the [core](https://www.core.edu.au/conference-portal) dataset to implement a search engine amongst journals and conferences, and articles in the domain of Computer Science.
The project is fully implemented in python, but uses html for printing purposes. Some javascript was used for failed attempts.

# Content of the repository
## Hiearchy
Python files are in the directory `script`.

Dataset datas can be found in the folder `data`. The folder `CORE_SET` contains the core data set, and a parsed version of it. The folder `metadata` should contain the s2orc data. The parsed datas are outputted in the `metadata_parsed` folder. The file `conference_data.json` is the data of keywords the conference set.

## Main files
If you want to update the dataset, you can probably use the file `script/download_s2orc_and_filter.py`.

To filter the data, use the file `script/parser.py`, by making sure the pathing is correct.

Finally, the files `script/search_article.py` and `script/search_conference.py` are used to search amongst article ans conference, respectively.

## Secondary files
The files `script/citation_distribution.py` and `script/rank_distribution` are used to print some graphs about the dataset.

# Example of usage
To filter amongst article, use

``python script/search_article n keyword1 keyword2 .. keywordm``

Where `n` is the maximum number of articles outputted (maximum 100), and `keyword1`,..,`keywordm` are keywords used to filter the result.
The output will be a file named `output/article_search_output.html`.

To filter amongst journals and conference, use

``python script/search_conference n keyword1 keyword2 .. keywordm``

Where `n` is the maximum number of conference outputted (maximum 100), and `keyword1`,..,`keywordm` are keywords used to filter the result.
The output will be a file named `output/conf_search_output.html`.

# Author
The project was realised by Samuel Bouaziz, as part of the [2.26.2 MPRI course](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-26-2).


# License
This project is Open Source.
