from tqdm import tqdm
import json
import nltk
import string

def find_rank(journal, journals):
    cleaned_journal = set(nltk.word_tokenize(journal))
    for journal in journals:
        journal_name = journal["name"]
        journal_acronym = journal["acronym"]
        cleaned_journal_name = set(nltk.word_tokenize(journal_name))
        if cleaned_journal_name.issubset(cleaned_journal):
            return journal["rank"],journal_name

    return "none","none"
