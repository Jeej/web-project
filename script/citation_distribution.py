from tqdm import tqdm
import json
import matplotlib.pyplot as plt
from statistics import mean



input_path = "data/metadata_parsed/metadata0.json"
data = []

with open(input_path, 'rb') as input:
    for line in tqdm(input.readlines()):
        metadata_dict = json.loads(line)
        nb_cit = metadata_dict["nb_cit"]
        data.append(nb_cit)

filtred_data=[0 for i in range(0,101)]

for elt in data:
    if elt>100:
        filtred_data[100]+=1
    else:
        filtred_data[elt]+=1

print(filtred_data)

plt.figure(1)
plt.plot(filtred_data)
plt.xlabel('Number of articles')
plt.ylabel('Number of citations')
plt.title('Distribution of the number of citations (top right is 100+)')

# plotting the data
#bins=[1,10,20,30,40,50,60,70,80,90,100]
plt.figure(2)
plt.hist(data,facecolor='b',bins=[0,1,2,3,4,5,6,7,8,9,9])

print(max(data))
print(mean(data))

plt.xlabel('Number of articles')
plt.ylabel('Number of citations')
plt.title('Distribution of the number of citations')
plt.show()
